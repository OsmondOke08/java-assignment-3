import java.util.Scanner;

class InvalidAgeException extends Exception{
    public InvalidAgeException(String str) {
        System.out.println(str);
    }
}

public class Problem1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter an Age:");
        int age = s.nextInt();

        try{
            if (age < 18 || age >= 60)
                throw new InvalidAgeException("The Age Enterred is out of the Range");

            else 
                System.out.println("The age enterred is within the range");
        }

        catch (InvalidAgeException a) {
            System.out.println(a);
        }
    }

}
