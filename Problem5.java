public class Problem5 {
    public static void main(String[] args) {
        String str = "How was your day today?";

        System.out.println("The Length Of The String is:" + str.length());
        System.out.println("The Length Of The String Without a is :" + str.replace("a", "").length());
        int charC = str.length() - str.replaceAll("a", "").length();

        System.out.println("The Number of Occurences of Character a In The String is: " + charC);
    }
}