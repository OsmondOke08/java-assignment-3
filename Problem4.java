import java.util.Scanner;

public class Problem4{ 

    public static void main(String args[])
    {
        String x, b = "";
        Scanner s = new Scanner(System.in);

        System.out.print("Enter Your Desired String To Be Tested:");

        x = s.nextLine();
        int n = x.length();
        for(int i = n - 1; i >= 0; i--)
        {
            b = b + x.charAt(i);
        }
        if(x.equalsIgnoreCase(b))
        {
            System.out.println("This String Is A Palindrome.");
        }
        else
        {
            System.out.println("This String Is NOT A Palindrome.");
        }
    }
}