import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 2; i++) {
            String name = "";
            int subA = 0;
            int subB = 0;
            int subC = 0;

            try {
                System.out.println("Enter Student name:" + name);
                    name = sc.nextLine();

                System.out.println("Enter Student grade A");
                
                if (sc. hasNextInt()){
                    
                   subA = sc.nextInt(); 
                   System.out.println("Grade A is:" + subA);
                    
                }
               

                else
                    throw new NumberFormatException();
                 
                System.out.println("Enter Student grade B");

                if (sc.hasNextInt()){ 

                    subB = sc.nextInt();
                   System.out.println("Enter grade B:" + subB);
                   
                }
                else    
                    throw new NumberFormatException();


                 System.out.println("Enter Student grade C");   

                if (sc.hasNextInt()){
                    subC = sc.nextInt();
                    System.out.println("Enter grade C:" + subC);
                    
                }

                else    
                    throw new NumberFormatException();

                

                if (subA < 0) throw new NegativeValueException();
                if (subA > 100) throw new ValueOutOfRangeException();

                if (subB < 0) throw new NegativeValueException();
                if (subB > 100) throw new ValueOutOfRangeException();

                if (subC < 0) throw new NegativeValueException();
                if (subC > 100) throw new ValueOutOfRangeException();

            } catch (ArithmeticException e) {
                System.out.println(e.getMessage());

            } catch (NegativeValueException e)  {
                System.out.println(e.getMessage());

            } catch (ValueOutOfRangeException e) {
                System.out.println(e.getMessage());
            }

            System.out.println("Name: " + name);
            System.out.println("Marks of subject A: " + subA);
            System.out.println("Marks of subject B: " + subB);
            System.out.println("Marks of subject C: " + subC);

            int total = subA + subB + subC;
            int avg = total/3;

            System.out.println("Average of Student is:" + avg);
        }

        sc.close();
    }
}